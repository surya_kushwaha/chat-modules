package com.aurigait.ejabberdchat;

/**
 * Created by Rahul Gupta on 4/14/2016.
 */
public class Constants
{

    public static final String PASSWORD = "password";
    public static final String DOMAIN = "domain";
    public static final String USER_JID = "jid";
    public static final String MESSAGE_DELIVERED = "delivered";
    public static final String MESSAGE_SENT = "sent";
    public static final String MESSAGE_READ = "read";
    public static final String GET_MESSAGE = "get_message";
    public static final String RECECEIVED_FROM = "received_from";

    public static final String MESSAGE_TYPE_OLD = "message_old";
    public static final String MESSAGE_TYPE_NEW = "message_new";
    public static final String USER = "user" ;
    public static final String RECEIVER = "receiver" ;
    public static final String PRESENCE = "presence";

}
