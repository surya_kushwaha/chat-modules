package com.aurigait.ejabberdchat;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUp extends AppCompatActivity implements View.OnClickListener {

    private Button btn_register,btn_cancel;
    private EditText et_userid,et_password,et_repassword,et_email;
    private String userid,password,email;
    public static boolean result=false;
    public static boolean processcompleted=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sign_up);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Sign Up");

        init();

    }
    private void init()
    {

        et_userid=(EditText)findViewById(R.id.et_userid);
        et_password=(EditText)findViewById(R.id.et_password);
        et_repassword=(EditText)findViewById(R.id.et_repassword);
        et_email=(EditText)findViewById(R.id.et_email);

        btn_register=(Button)findViewById(R.id.btn_signup);
        btn_cancel=(Button)findViewById(R.id.btn_cancel);
        btn_register.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);

    }
    @Override
    public void onClick(View V)
    {
      switch (V.getId())
      {
          case R.id.btn_signup:




              if(validate()==true)
              {
                  if(!isConnectingToInternet())
                  {
                      new AlertDialog.Builder(SignUp.this)
                              .setTitle("Connection Error")
                              .setMessage("Please check your internet connection.")
                              .setCancelable(false)
                              .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {
                                      dialog.dismiss();
                                  }
                              })
                              .show();
                      return ;
                  }
                  try
                  {
                      new Registration(this,userid,password,email).perform();
                      Log.e("RESULT", result+"");
                  }catch (Exception e)
                  {
                      Log.e("Error", "REGISTRATION FAIL"); result=false;processcompleted=true;
                  }

                  while (processcompleted==false)
                  {

                  }
              if(result==true)
              {     //startActivity(new Intent(SignUp.this, LoginActivity.class));
                  Toast.makeText(getApplicationContext(), "Signup Sucessfull", Toast.LENGTH_SHORT).show();
                  this.finish();
              }
              else
                  Toast.makeText(getApplicationContext(), getResources().getString(R.string.signupfail), Toast.LENGTH_LONG).show();

              }


              //.perform();
              break;
          case R.id.btn_cancel:
              //startActivity( new Intent(SignUp.this, LoginActivity.class));
              Toast.makeText(getApplicationContext(), getResources().getString(R.string.signupcancel), Toast.LENGTH_LONG).show();

              finish();
              break;
      }
    }
    private boolean validate()
    {

       userid=et_userid.getText().toString();
       password=et_password.getText().toString();
        String confirmPassword=et_repassword.getText().toString();
       email=et_email.getText().toString();
        if(userid==null || verifyJabberID(userid)!=true)
        {
            et_userid.setText("Invalid Jabber Id(format like: user@localhost)");
            et_userid.requestFocus();
            return false;
        }

        if(password==null || !password.equals(confirmPassword))
        {
           if(password==null)
               et_password.setError("password can't be empty");
           else
               et_password.setError("password and confirm password should be same");
            et_password.requestFocus();

            return  false;
        }
        if(email==null)
        {
            et_email.setError("email can't be empty");
            et_email.requestFocus();
            return false;
        }
        boolean valid = isValidEmail(email);
        if(valid==false)
            return valid;
        return true;
    }
    public static boolean verifyJabberID(String jid){
        try {
            String parts[] = jid.split("@");
            if (parts.length != 2 || parts[0].length() == 0 || parts[1].length() == 0){
                return false;
            }
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }

    public boolean isConnectingToInternet(){
        boolean connected=false;
        ConnectivityManager connectivityManager;
        try {
            connectivityManager = (ConnectivityManager) getApplicationContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() &&
                    networkInfo.isConnected();
            return connected;


        } catch (Exception e) {
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
            Log.v("connectivity", e.toString());
        }
        return connected;
    }
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
