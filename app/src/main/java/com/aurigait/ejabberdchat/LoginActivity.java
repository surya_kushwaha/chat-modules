package com.aurigait.ejabberdchat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button;
    private EditText edPW;
    private EditText edJID;
    public String host=null;
    public String port=null;
    public int MY_PERMISSIONS_REQUEST_READ_CONTACTS=0;
    public static ProgressDialog loginProgressDialog;
    ProgressBar progressBar;
    Context context=this;
    int t=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("eJabberd");

        String Password = PreferenceManager.getDefaultSharedPreferences(this).getString("xmpp_password", null);
        String Service = PreferenceManager.getDefaultSharedPreferences(this).getString("xmpp_jid", null);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);

        if(Password!=null && Service!=null)
        {
            if(isConnectingToInternet(getApplicationContext())==false)
            {
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Connection Error")
                        .setMessage("Please check your internet connection.")
                        .setCancelable(false)
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                return;
            }
        }

        button = (Button)this.findViewById(R.id.signIn);

        edPW = (EditText)this.findViewById(R.id.et_password);
        edJID = (EditText)this.findViewById(R.id.et_userid);

        if(Password != null){
            edPW.setText(Password);
        }
        if(Service != null){
            edJID.setText(Service);
        }

        button.setOnClickListener(this);

        findViewById(R.id.signUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(LoginActivity.this, SignUp.class));
            }
        });
    }

    @Override
    public void onClick(View v) {

        if(!isConnectingToInternet(getApplicationContext()))
        {
            new AlertDialog.Builder(LoginActivity.this)
                    .setTitle("Connection Error")
                    .setMessage("Please check your internet connection.")
                    .setCancelable(false)
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
            return ;
        }
        MyService.connectAgain(LoginActivity.this,edJID.getText().toString(),edPW.getText().toString());
        Intent i = new Intent(LoginActivity.this,MyService.class);
        startService(i);
        //startActivity(new Intent(LoginActivity.this,MainActivity.class));
    }

    public static boolean isConnectingToInternet(Context context){
        boolean connected=false;
        ConnectivityManager connectivityManager;
        try {
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() &&
                    networkInfo.isConnected();
            return connected;


        } catch (Exception e) {
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
            Log.v("connectivity", e.toString());
        }
        return connected;
    }
}
