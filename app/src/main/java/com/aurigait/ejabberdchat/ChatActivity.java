package com.aurigait.ejabberdchat;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smackx.iqlast.LastActivityManager;
import org.jivesoftware.smackx.iqlast.packet.LastActivity;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private EditText et_message;
    private ImageButton btn_send;
    public static ArrayList<ChatMessageModel> chatList;
    public static ArrayList<ChatMessageModel> unReadChatList;
    private String user1 = "user A", user2 = "user A";
    private Random random;
    public static ChatAdapter chatAdapter;
    private MyService mService;
    private Users mReceiverUser;
    Random notificationId = new Random();
    final int notifyId = notificationId.nextInt();
    private ChatMessageModel previousMessage;
    private LastActivity activity;
    public boolean canLoadMore = true;
    private int lastMessageScroll;

    @Override
    protected void onCreate(@Nullable
                                    Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mReceiverUser = (Users) getIntent().getSerializableExtra("user");
        MainActivity.unReadCount.put(mReceiverUser.getJabberID().split("/")[0].split("@")[0],0);
        setLastSeenWithUser();

        bindUI();
        setRecycylerView();
        DatabaseHelper.getInstanse(getApplicationContext()).updateMessageReadType(mReceiverUser.getJabberID().split("/")[0].split("@")[0],Constants.MESSAGE_TYPE_OLD);


        setAdapter();

        updateReadStatus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setLastSeenWithUser() {

        if(MyXMPP.connection.isAuthenticated())
        {
            try {
                activity  = LastActivityManager.getInstanceFor(MyXMPP.connection).getLastActivity(mReceiverUser.getJabberID())   ;
            } catch (SmackException.NoResponseException e) {
                e.printStackTrace();
            } catch (XMPPException.XMPPErrorException e) {
                e.printStackTrace();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
            long time= activity.getIdleTime();
            Log.e("lasttime", String.valueOf(time));
            if(time>0)
            {
                long lastSeen = (long) System.currentTimeMillis() - time;
                Date d = new Date(lastSeen);
                setTitle(mReceiverUser.getName());
                getSupportActionBar().setSubtitle("last seen at "+getLastSeen(d));
            }
            else if(time<0){
                setTitle(mReceiverUser.getName());
                getSupportActionBar().setSubtitle(" ");
            }
            else
            {
                setTitle(mReceiverUser.getName());
                getSupportActionBar().setSubtitle("online");
            }
        }
    }

    private String getDate(String date){

        if(date == null){
          return null;
        }
        String timeinMilis="";
        try {
            Calendar c = toLocale(date);
            if(c != null){
                timeinMilis = String.valueOf(c.getTimeInMillis());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return timeinMilis;
    }

    public static Calendar toLocale(String date_time) throws Exception {

        DateFormat UTC_DATE_TIME_FORMAT_017 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);
        if (date_time == null)
            return null;

        UTC_DATE_TIME_FORMAT_017.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date utcDate = UTC_DATE_TIME_FORMAT_017.parse(date_time);

        UTC_DATE_TIME_FORMAT_017.setTimeZone(TimeZone.getDefault());
        String dateUtc = UTC_DATE_TIME_FORMAT_017.format(utcDate);

        Date localeDate = UTC_DATE_TIME_FORMAT_017.parse(dateUtc);
        //localeDate.getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(localeDate);

        return calendar;
    }

    private String getLastSeen(Date date){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        return simpleDateFormat.format(date);
    }

    synchronized private void updateReadStatus() {
        if(MyXMPP.connected){
        unReadChatList = DatabaseHelper.getInstanse(getApplicationContext()).getUnreadMessages(mReceiverUser.getJabberID().split("/")[0].split("@")[0]);
        if(unReadChatList != null && unReadChatList.size()>0){

            for(ChatMessageModel chatModel : unReadChatList){

                Log.v("user", MyXMPP.connection.getUser().split("/")[0]);
                chatModel.setMessage("read");

                    if(getmService().xmpp.sendMessageReadStatus(chatModel)){
                        DatabaseHelper.getInstanse(getApplicationContext()).updateMessage(chatModel.getMessageId(), Constants.MESSAGE_READ,
                                                                                          String.valueOf(System.currentTimeMillis()), "", "");

                    }
                }
            }
        }
    }

    private void setAdapterwithHistory(){

    }

    private void setAdapter() {
        chatList = new ArrayList<>();
        chatList = DatabaseHelper.getInstanse(getApplicationContext()).getMessages(mReceiverUser.getJabberID().split("/")[0].split("@")[0]);
       // Collections.sort(chatList, new CustomComparator());

        chatAdapter = new ChatAdapter(this, chatList, Preferences.getPreferences(getApplicationContext(), Constants.USER, ""));
        recyclerView.setAdapter(chatAdapter);
        lastMessageScroll = (chatAdapter.getItemCount()-1) >0 ?chatAdapter.getItemCount()-1 :0;

        if(MyXMPP.connection.isAuthenticated()) {

            getmService().xmpp.chatCreate(mReceiverUser.getJabberID());

        }

        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                recyclerView.scrollToPosition(lastMessageScroll);
            }
        });

    }

    private void setRecycylerView() {

        random = new Random();
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void bindUI() {
        recyclerView = (RecyclerView) findViewById(R.id.chat_recyclerView);
        et_message = (EditText) findViewById(R.id.et_message);
        btn_send = (ImageButton) findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:

                sendMessage();
                break;
        }
    }

    private void sendMessage() {

        String message = et_message.getText().toString().trim();
        if (!message.equalsIgnoreCase("")) {
            final ChatMessageModel chatMessageModel = new ChatMessageModel();
            String packetID = "";
            packetID = packetID + String.valueOf(System.currentTimeMillis()) + getRandomString() + "AN";
            Log.v("user", MyXMPP.connection.getUser());
            chatMessageModel.setMessageFrom(Preferences.getPreferences(getApplicationContext(),Constants.USER,""));
            chatMessageModel.setMessageTo(mReceiverUser.getJabberID().split("/")[0]);
            chatMessageModel.setMessage(message);
            chatMessageModel.setMessageId(packetID);
            chatMessageModel.setMessageReadTime("");
            chatMessageModel.setMessageDeliveredTime("");
            chatMessageModel.setMessageSentTime(String.valueOf(System.currentTimeMillis()));
            chatMessageModel.setMessageStatus(Constants.MESSAGE_SENT);
            chatMessageModel.setMessageType(Constants.MESSAGE_TYPE_OLD);

            et_message.setText("");

            if(MyXMPP.connected){
                if(MyXMPP.connection.isAuthenticated()){
                    getmService().xmpp.sendMessage(chatMessageModel);
                    chatMessageModel.setMessageTo(mReceiverUser.getJabberID().split("/")[0].split("@")[0]);
                    DatabaseHelper.getInstanse(getApplicationContext()).insertMessageFieldsValue(chatMessageModel);

                    updateChatList(chatMessageModel);
                }
                else {
                    Toast.makeText(ChatActivity.this, "User not logged in", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(ChatActivity.this, "not connected", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public MyService getmService() {
        return mService;
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver,
                                                                                    new IntentFilter(Constants.GET_MESSAGE));

        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    @Override
    public void onStop() {
        super.onStop();
        try {

            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            ChatMessageModel chatMessageModel = (ChatMessageModel) intent.getSerializableExtra(Constants.RECECEIVED_FROM);
            boolean updatePresence = (boolean) intent.getSerializableExtra(Constants.PRESENCE);
            if(chatMessageModel != null ){
                updateChatList(chatMessageModel);
            }
            if(updatePresence){
                setLastSeenWithUser();
            }

        }
    };


    private void updateChatList(ChatMessageModel chatMessageModel) {
        if(chatMessageModel != null)
        {
            if(chatMessageModel.getMessageFrom().split("/")[0].split("@")[0].equals(mReceiverUser.getJabberID().split("/")[0].split("@")[0]))
            {
                if(!chatMessageModel.getMessageStatus().equals(Constants.MESSAGE_READ)){
                    chatAdapter.add(chatMessageModel);
                }

                chatAdapter.notifyDataSetChanged();
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        // Select the last row so it will scroll into view...
                        recyclerView.scrollToPosition(chatAdapter.getItemCount()-1);
                    }
                });
                DatabaseHelper.getInstanse(getApplicationContext()).updateMessageReadType(mReceiverUser.getJabberID().split("/")[0].split("@")[0],Constants.MESSAGE_TYPE_OLD);
                updateReadStatus();
                DatabaseHelper.getInstanse(getApplicationContext()).updateMessage(chatMessageModel.getMessageId(), Constants.MESSAGE_READ,
                                                                                  String.valueOf(System.currentTimeMillis()), "", "");
            }
            else if(chatMessageModel.getMessageTo().split("/")[0].split("@")[0].equals(mReceiverUser.getJabberID().split("/")[0].split("@")[0]))
            {
                if(chatMessageModel.getMessageStatus().equals(Constants.MESSAGE_READ)){
                    chatList = DatabaseHelper.getInstanse(getApplicationContext()).getMessages(mReceiverUser.getJabberID().split("/")[0].split("@")[0]);
                    chatAdapter.updateListItem(chatList);
                    chatAdapter.notifyDataSetChanged();
                    recyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            // Select the last row so it will scroll into view...
                            recyclerView.scrollToPosition(chatAdapter.getItemCount()-1);
                        }
                    });
                }
                else {
                    chatAdapter.add(chatMessageModel);
                    chatAdapter.notifyDataSetChanged();
                    recyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            // Select the last row so it will scroll into view...
                            recyclerView.scrollToPosition(chatAdapter.getItemCount()-1);
                        }
                    });
                    updateReadStatus();
                }
               // DatabaseHelper.getInstanse(getApplicationContext()).updateMessageReadType(mReceiverUser.getJabberID(),Constants.MESSAGE_TYPE_OLD);

            }

            else {
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ChatActivity.this);
                NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                    mBuilder.setLargeIcon(largeIcon);
                } else {
                    mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                    mBuilder.setLargeIcon(largeIcon);
                }

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                mBuilder.setContentTitle("New message from" + DatabaseHelper.getInstanse(getApplicationContext())
                        .getUserById(chatMessageModel.getMessageFrom().split("/")[0].split("@")[0]))
                        .setContentText(chatMessageModel.getMessage());
                mBuilder.setSound(defaultSoundUri);

                mNotifyManager.notify(notifyId, mBuilder.build());
                mBuilder.setAutoCancel(false);
            }
        }
    }

    public String getRandomString(){
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
    @Override
    public void onBackPressed() {
        getmService().xmpp.chatDestroy();
        super.onBackPressed();
    }

    /*class GetConversation implements Runnable{

        @Override
        public void run() {
            if(MyXMPP.connection.isAuthenticated())
            {
               //
            }
        }
    }*/
}
