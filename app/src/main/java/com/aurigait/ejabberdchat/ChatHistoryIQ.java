package com.aurigait.ejabberdchat;


import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.IQ;


public class ChatHistoryIQ extends IQ {

    public XMPPConnection connection;
    public static final String NAMESPACE = "urn:jabxmpp:message";

    private String from,to,type,date;
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ChatHistoryIQ() {
        super("query", NAMESPACE);
    }

    public void setValues(String to,String type){
        this.from = null;
        this.to = to; // self user to get unread messages
        this.type = type;
        this.date = null;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();
        xml.append("<ToJid>").append(to)
           .append("</ToJid>").append("<type>")
           .append(type).append("</type>");
        if(from != null){
            xml.append("<FromJID>").append(from).append("</FromJID>");
        }

        return xml;
    }


}
