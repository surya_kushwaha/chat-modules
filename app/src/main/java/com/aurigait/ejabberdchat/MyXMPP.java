package com.aurigait.ejabberdchat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;


import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.filter.StanzaFilter;
import org.jivesoftware.smack.filter.StanzaTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.carbons.CarbonManager;
import org.jivesoftware.smackx.carbons.packet.CarbonExtension;
import org.jivesoftware.smackx.delay.packet.DelayInformation;

import java.io.IOException;
import java.util.Date;

public class MyXMPP {


    public static boolean connected = false;
    public boolean loggedin = false;
    public static boolean isconnecting = false;
    public static boolean isToasted = true;
    private boolean chat_created = false;
    private String serverAddress;
    public static XMPPTCPConnection connection;
    public static String loginUser;
    public static String passwordUser;
    MyService context;
    public static MyXMPP instance = null;
    public static boolean instanceCreated = false;
    public static MyService.Callbacks callbacks;
    private static Activity activity;

    public org.jivesoftware.smack.chat.Chat Mychat;

    ChatManagerListenerImpl mChatManagerListener;
    MMessageListener mMessageListener;

    String text = "";
    String mMessage = "", mReceiver = "";
    StanzaFilter typeFilter = new StanzaTypeFilter(Message.class);

    StanzaListener sycStanzaListener = new StanzaListener() {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            Log.i("packet",packet.toString());
            if(packet instanceof Presence){
                Presence presence = (Presence)packet;

                updatePresence(presence);
            }
        }
    };

    public void updatePresence(Presence presence)
    {
        Intent backIntent = new Intent(Constants.GET_MESSAGE);
        ChatMessageModel chatMessageModel = null;
        backIntent.putExtra(Constants.RECECEIVED_FROM, chatMessageModel);
        backIntent.putExtra(Constants.PRESENCE, true);
        LocalBroadcastManager.getInstance(context).sendBroadcast(backIntent);

        if(callbacks != null){
            callbacks.presenceUpdate(presence);
        }
    }
    public MyXMPP(MyService context, String serverAdress, String logiUser,String passwordser,Activity activity) {
        this.serverAddress = serverAdress;
        this.loginUser = logiUser;
        this.passwordUser = passwordser;
        this.context = context;
        this.activity = activity;
        init();
    }

    public static void registerClient(Activity activity){
        callbacks = (MyService.Callbacks)activity;
    }

    public static MyXMPP getInstance(MyService context, String server, String user, String pass, Activity activity) {

        if (instance == null)
        {
            instance = new MyXMPP(context, server, user, pass,activity);
            instanceCreated = true;
        }
        return instance;
    }

    static {
        try {
            Class.forName("org.jivesoftware.smack.ReconnectionManager");
        } catch (ClassNotFoundException ex) {
            // problem loading reconnection manager
        }
    }

    public void init() {
        mMessageListener = new MMessageListener(context);
        mChatManagerListener = new ChatManagerListenerImpl();
        initialiseConnection();
    }

    private void initialiseConnection() {

        XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration
                .builder();
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        config.setServiceName("localhost");
        config.setHost(serverAddress);
        config.setPort(5222);
        config.setDebuggerEnabled(true);
        config.setCompressionEnabled(false);
        //config.setUsernameAndPassword(loginUser, passwordUser);
        XMPPTCPConnection.setUseStreamManagementResumptiodDefault(true);
        XMPPTCPConnection.setUseStreamManagementDefault(true);
        connection = new XMPPTCPConnection(config.build());

        XMPPConnectionListener connectionListener = new XMPPConnectionListener();

        connection.addConnectionListener(connectionListener);
        //connection.addPacketListener(packetListener, null);
        connection.addSyncStanzaListener(sycStanzaListener,null);

                ChatHistoryProvider chatHistoryProvider = new ChatHistoryProvider();
                ProviderManager.addIQProvider("query", ChatHistoryIQ.NAMESPACE, chatHistoryProvider);
    }

    public void disconnect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                connection.disconnect();
            }
        }).start();
    }

    public void connect(final String caller)
    {

        AsyncTask<Void, Void, Boolean> connectionThread = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected synchronized Boolean doInBackground(Void... arg0) {

                if (connection!= null && connection.isConnected())
                    return false;
                isconnecting = true;
                if (isToasted)
                    new Handler(Looper.getMainLooper()).post(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(context,
                                           caller + "=>connecting....",
                                           Toast.LENGTH_LONG).show();
                        }
                    });
                Log.d("Connect() Function", caller + "=>connecting....");

                try {
                    connection.connect();

                    Log.d("status", "connection");

                    connected = true;

                } catch (IOException e) {
                    if (isToasted)
                        new Handler(Looper.getMainLooper())
                                .post(new Runnable() {

                                    @Override
                                    public void run()
                                    {
                                        Toast.makeText(context,"(" + caller + ")"+ "IOException: ",Toast.LENGTH_SHORT).show();
                                    }
                                });
                    connected = false;

                    Log.e("(" + caller + ")", "IOException: " + e.getMessage());
                } catch (SmackException e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(context,"(" + caller + ")" + "SMACKException: ", Toast.LENGTH_SHORT).show();
                        }
                    });
                    Log.e("(" + caller + ")","SMACKException: " + e.getMessage());
                    connected = false;
                } catch (XMPPException e) {
                    if (isToasted)

                        new Handler(Looper.getMainLooper())
                                .post(new Runnable() {

                                    @Override
                                    public void run() {

                                        Toast.makeText(context,"(" + caller + ")"+ "XMPPException: ",Toast.LENGTH_SHORT).show();

                                    }
                                });
                    Log.e("connect(" + caller + ")","XMPPException: " + e.getMessage());
                    connected = false;
                }
                return isconnecting = false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);

            }
        };
        connectionThread.execute();
    }

    public void login() throws XMPPException, SmackException, IOException, InterruptedException {

        connection.login(loginUser, passwordUser, Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        CarbonManager.getInstanceFor(connection).enableCarbons();
        Presence presencePacket = new Presence(Presence.Type.available);
        connection.sendStanza(presencePacket);//sendPacket(presencePacket);

    }


    private class ChatManagerListenerImpl implements ChatManagerListener {
        @Override
        public void chatCreated(final org.jivesoftware.smack.chat.Chat chat,
                                final boolean createdLocally) {
            if (!createdLocally)
                chat.addMessageListener(mMessageListener);
        }
    }

    public void chatCreate(String messageTo){
        Mychat = ChatManager.getInstanceFor(connection).createChat(
                messageTo,
                mMessageListener);
        chat_created = true;
    }

    public void chatDestroy(){
        if(Mychat != null){
            Mychat.removeMessageListener(mMessageListener);
            Mychat = null;
            chat_created = false;
        }
    }

    public void sendMessage(ChatMessageModel chatMessage) {

        if (!chat_created) {
            Mychat = ChatManager.getInstanceFor(connection).createChat(
                    chatMessage.getMessageTo(),
                    mMessageListener);
            chat_created = true;
        }
        final Message message = new Message();
        message.setBody(chatMessage.getMessage());
        message.setStanzaId(chatMessage.getMessageId());
        message.setType(Message.Type.chat);
        message.setSubject("chat");
        try {
            if (connection.isAuthenticated()) {

                Mychat.sendMessage(message);
                //Thread.sleep(500);
                Log.d("status", "connection.isAuth");

            }
        } catch (NotConnectedException e) {
            Log.e("xmpp.SendMessage()", "msg Not sent!-Not Connected!");

        } catch (Exception e) {
            Log.e("xmpp.SendMessage()",
                  "msg Not sent!" + e.getMessage());
        }
    }

    public boolean sendMessageReadStatus(ChatMessageModel chatMessage) {

        if (!chat_created) {
            Mychat = ChatManager.getInstanceFor(connection).createChat(
                    chatMessage.getMessageTo());
            chat_created = true;
        }
        final Message message = new Message();
        message.setBody(chatMessage.getMessage());
        message.setStanzaId(chatMessage.getMessageId());
        message.setType(Message.Type.chat);
        message.setSubject("read");

        try {
            if (connection.isAuthenticated()) {

                Mychat.sendMessage(message);
                //Thread.sleep(500);
                Log.d("status", "connection.isAuth");
                return true;

            }
        } catch (NotConnectedException e) {
            Log.e("xmpp.SendMessage()", "msg Not sent!-Not Connected!");

        } catch (Exception e) {
            Log.e("xmpp.SendMessage()",
                  "msg Not sent!" + e.getMessage());
        }
        return false;
    }

    public class XMPPConnectionListener implements ConnectionListener {
        @Override
        public void connected(final XMPPConnection connection) {

            Log.d("xmpp", "Connected!");
            context.startActivity(new Intent(context,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

            connected = true;
            if (!connection.isAuthenticated()) {
                try {

                    login();
                } catch (XMPPException e) {
                    e.printStackTrace();
                } catch (SmackException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void connectionClosed() {
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub

                        Toast.makeText(context, "ConnectionCLosed!",
                                       Toast.LENGTH_SHORT).show();

                    }
                });
            Log.d("xmpp", "ConnectionCLosed!");

            connected = false;
            //callbacks.connectionUpdate(connected);
            chat_created = false;
            loggedin = false;
        }

        @Override
        public void connectionClosedOnError(Exception arg0) {
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(context, "ConnectionClosedOn Error!!",
                                       Toast.LENGTH_SHORT).show();

                    }
                });
            Log.d("xmpp", "ConnectionClosedOn Error!");
            connected = false;
            //callbacks.connectionUpdate(connected);
            chat_created = false;
            loggedin = false;
        }

        @Override
        public void reconnectingIn(int arg0) {

            Log.d("xmpp", "Reconnectingin " + arg0);

            loggedin = false;
        }

        @Override
        public void reconnectionFailed(Exception arg0) {
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {

                        Toast.makeText(context, "ReconnectionFailed!",
                                       Toast.LENGTH_SHORT).show();

                    }
                });
            Log.d("xmpp", "ReconnectionFailed!");
            connected = false;
            // callbacks.connectionUpdate(connected);
            chat_created = false;
            loggedin = false;
        }

        @Override
        public void reconnectionSuccessful() {
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub

                        Toast.makeText(context, "REConnected!",
                                       Toast.LENGTH_SHORT).show();

                    }
                });
            Log.d("xmpp", "ReconnectionSuccessful");
            connected = true;

            chat_created = false;
            loggedin = false;
        }

        @Override
        public void authenticated(XMPPConnection arg0, boolean arg1) {
            Log.d("xmpp", "Authenticated!");
            loggedin = true;

            //activity.finish();
            Preferences.setPreferences(context,Constants.USER,loginUser);
            Preferences.setPreferences(context,Constants.PASSWORD,passwordUser);

            ChatManager.getInstanceFor(connection).addChatListener(
                    mChatManagerListener);
            if(callbacks != null){
                callbacks.connectionUpdate(connection.isAuthenticated());
            }

            chat_created = false;
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            }).start();
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub

                        Toast.makeText(context, "Authenticate Connected!",
                                       Toast.LENGTH_SHORT).show();

                    }
                });
        }
    }

    private class MMessageListener implements ChatMessageListener {

        public MMessageListener(Context contxt) {
        }

        @Override
        public void processMessage(final org.jivesoftware.smack.chat.Chat chat,
                                   Message message) {
            Log.i("MyXMPP_MESSAGE_LISTENER", "Xmpp message received: '"
                    + message);
            CarbonExtension packetExtension = message.getExtension("sent","urn:xmpp:carbons:2");
            if(packetExtension != null && packetExtension.getDirection().name().equals("sent"))
            {
                message = (Message)packetExtension.getForwarded().getForwardedPacket();
            }
            if (message.getType() == Message.Type.chat && message.getBody() != null)
            {
                Intent backIntent = new Intent(Constants.GET_MESSAGE);

                if ("read".equals(message.getSubject()))
                {

                    DatabaseHelper.getInstanse(context).updateMessage(message.getStanzaId(),Constants.MESSAGE_READ,
                                                                      String.valueOf(System.currentTimeMillis()),"","");

                    ChatMessageModel chatMessageModel = DatabaseHelper.getInstanse(context).getMessageDetailsFromMessageID(message.getStanzaId());

                    chatMessageModel.setMessageStatus(Constants.MESSAGE_READ);
                    chatMessageModel.setMessageReadTime(String.valueOf(System.currentTimeMillis()));

                    backIntent.putExtra(Constants.RECECEIVED_FROM, chatMessageModel);
                    backIntent.putExtra(Constants.PRESENCE, false);
                    //LocalBroadcastManager.getInstance(context).sendBroadcast(backIntent);
                }
                else
                {
                    ChatMessageModel chatMessageModel = new ChatMessageModel();

                    chatMessageModel.setMessageFrom(message.getFrom().split("/")[0].split("@")[0]);
                    chatMessageModel.setMessage(message.getBody());
                    chatMessageModel.setMessageId(message.getStanzaId());
                    chatMessageModel.setMessageTo(message.getTo().split("/")[0].split("@")[0]);
                    chatMessageModel.setMessageReadTime("");

                    chatMessageModel.setMessageDeliveredTime(String.valueOf(System.currentTimeMillis()));
                    chatMessageModel.setMessageSentTime(String.valueOf(System.currentTimeMillis()));//(String.valueOf(getDelayTimeStamp(message)));
                    chatMessageModel.setMessageStatus(Constants.MESSAGE_DELIVERED);
                    chatMessageModel.setMessageType(Constants.MESSAGE_TYPE_NEW);

                    DatabaseHelper.getInstanse(context).insertMessageFieldsValue(chatMessageModel);

                    //Intent backIntent = new Intent(Constants.GET_MESSAGE);
                    backIntent.putExtra(Constants.RECECEIVED_FROM, chatMessageModel);
                    backIntent.putExtra(Constants.PRESENCE, false);

                }
                LocalBroadcastManager.getInstance(context).sendBroadcast(backIntent);
            }

        }
    }

    Date date;
    private long getDelayTimeStamp(Message message) {
        try {

            DelayInformation inf = null;
            try {
                inf = (DelayInformation) message.getExtension("urn:xmpp:delay");
            } catch (Exception e) {
                e.printStackTrace();
            }
            // get offline message timestamp
            if (inf != null) {
                date = inf.getStamp();
                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub

                        Toast.makeText(context, "delay "+ date.getTime(),
                                       Toast.LENGTH_SHORT).show();

                    }
                });
                return date.getTime();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                Toast.makeText(context, "new date "+ new Date().getTime(),
                               Toast.LENGTH_SHORT).show();

            }
        });
        return new Date().getTime();
    }
}
