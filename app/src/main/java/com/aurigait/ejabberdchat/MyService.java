package com.aurigait.ejabberdchat;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.IBinder;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.Presence;

public class MyService extends Service
{
    //private static final String DOMAIN = "192.168.1.102";//"192.168.3.89";
    private static final String DOMAIN = "192.168.3.89";
    private static String USERNAME = "";
    private static String PASSWORD = "";
    public static ConnectivityManager cm;
    public static MyXMPP xmpp;
    public static boolean ServerchatCreated = false;
    String text = "";
    private static Activity ACTIVITY;

    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if(!USERNAME.isEmpty() && !PASSWORD.isEmpty() && ACTIVITY != null){
            xmpp = MyXMPP.getInstance(MyService.this, DOMAIN, USERNAME, PASSWORD,ACTIVITY);
            if(MyXMPP.connection != null){
                xmpp.connect("onCreate");
            }
            else {
                xmpp.connect("onCreate");
            }

        }
    }

    public static void connectAgain(Activity activity,String userName,String password){
        USERNAME = userName;
        PASSWORD = password;
        ACTIVITY = activity;
    }

    /*@Override
    public int onStartCommand(final Intent intent, final int flags,
                              final int startId) {
        //this.USERNAME = intent.getStringExtra(Constants.USER);
        //this.PASSWORD = intent.getStringExtra(Constants.PASSWORD);


        return Service.START_NOT_STICKY;
    }*/

    @Override
    public boolean onUnbind(final Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy()
    {
        USERNAME = "";
        PASSWORD = "";
        super.onDestroy();
    }

    public static boolean isNetworkConnected() {
        return cm.getActiveNetworkInfo() != null;
    }

    public interface Callbacks{
        public void connectionUpdate(boolean mConnection);
        public void presenceUpdate(Presence presence);
    }

    public static void registerClient(Activity activity){
        MyXMPP.registerClient(activity);
    }
}