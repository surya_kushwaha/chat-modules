package com.aurigait.ejabberdchat;

import java.io.Serializable;

public class Users implements Serializable
{

    String name;
    private String jabberID;
    private boolean presence;

    public boolean isPresence() {
        return presence;
    }

    public void setPresence(boolean presence) {
        this.presence = presence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJabberID() {
        return jabberID;
    }

    public void setJabberID(String jabberID) {
        this.jabberID = jabberID;
    }
}
