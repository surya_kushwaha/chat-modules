package com.aurigait.ejabberdchat;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.test.espresso.core.deps.guava.reflect.TypeToken;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterLoadedListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class MainActivity extends AppCompatActivity implements MyService.Callbacks {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    public static ArrayList<Users> rosterList;
    public static UserAdapter userAdapter;
    private MyService mService;
    private static final String TAG = "MainActivity";
    public Button btn;
    private final int REQUEST_PERMISSION = 1;
    private DatabaseHelper databaseHelper;

    Random notificationId = new Random();
    final int notifyId = 1024;

    public static HashMap<String, Boolean> presenceUser = new HashMap<String, Boolean>();
    public static HashMap<String, Integer> unReadCount = new HashMap<String, Integer>();
    ArrayList<HistoryModel> historyMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button) findViewById(R.id.btn);
        btn.setVisibility(View.GONE);

        databaseHelper = DatabaseHelper.getInstanse(getApplicationContext());
        databaseHelper.openDatabase();

        MyService.registerClient(MainActivity.this);
        setRecyclerView();

        /*IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectionReceiver, filter);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);//Menu Resource, Menu
        return true;
    }
    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_signout:
                if(MyXMPP.connection.isConnected()){
                    try
                    {
                        MyXMPP.connection.disconnect(new Presence(Presence.Type.unavailable));
                        MyXMPP.connection.instantShutdown();
                        //MyXMPP.connection = null;
                    }
                    catch(SmackException.NotConnectedException e)
                    {
                        e.printStackTrace();
                    }
                    MyXMPP.registerClient(null);
                    Intent i = new Intent(MainActivity.this,MyService.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    stopService(i);
                    finish();
                    //startActivity(new Intent(this,LoginActivity.class));

                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    @Override
    public void onBackPressed()
    {
        if(MyXMPP.connection.isConnected()){
            Presence presencePacket = new Presence(Presence.Type.available);
            try
            {
                MyXMPP.connection.sendStanza(presencePacket);
            }
            catch(SmackException.NotConnectedException e)
            {
                e.printStackTrace();
            }
        }
        super.onBackPressed();
    }

    public void getRoster(final XMPPTCPConnection connection) {
        try {
            Roster roster = Roster.getInstanceFor(connection);
            roster.setSubscriptionMode(Roster.SubscriptionMode.manual);
            roster.addRosterLoadedListener(new RosterLoadedListener() {
                @Override
                public void onRosterLoaded(Roster roster) {
                    Collection<RosterEntry> entries = roster.getEntries();
                    presenceUser.clear();
                    Presence presence;
                    for (RosterEntry entry : entries) {
                        Log.d("entry", String.valueOf(entry));
                        Log.i("user",entry.getUser());
                        //Log.i("name",entry.getName());
                        presence = roster.getPresence(entry.getUser());
                        presenceUser.put(entry.getUser(), presence.isAvailable());
                        unReadCount.put(entry.getUser().split("/")[0].split("@")[0],0);

                        if (!databaseHelper.checkExistanceJID(entry.getUser())) {
                            // insert jid and name
                            databaseHelper.insertFieldsValue(entry.getUser(),entry.getUser().split("/")[0].split("@")[0]);
                        } else {
                            databaseHelper.updateUsers(entry.getUser(),entry.getUser().split("/")[0].split("@")[0]);
                        }
                    }

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            rosterList = databaseHelper.getUsers();
                            userAdapter.setRosterList(rosterList);
                            userAdapter.notifyDataSetChanged();

                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setRecyclerView() {

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        rosterList = new ArrayList<>();
        if (databaseHelper.getUsers().size() > 0)
        {
            rosterList = databaseHelper.getUsers();
        }
        userAdapter = new UserAdapter(MainActivity.this, rosterList);
        recyclerView.setAdapter(userAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseHelper.closeDatabase();
       // unregisterReceiver(connectionReceiver);
        finish();
    }

    @Override
    public void connectionUpdate(boolean mConnection) {
        final boolean conn = mConnection;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "" + conn, Toast.LENGTH_SHORT).show();
            }
        });
        if (mConnection) {
            getRoster(MyXMPP.connection);
            getUnreadMessage();
        }
    }

    private void getUnreadMessage() {
        ChatHistoryIQ myIQ = new ChatHistoryIQ();

        myIQ.setValues(MyXMPP.connection.getUser().split("/")[0].split("@")[0], "record");
        if (MyXMPP.connection.isAuthenticated()) {

            try {
                MyXMPP.connection.sendIqWithResponseCallback(myIQ, new StanzaListener() {
                    @Override
                    public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
                        if (packet instanceof ChatHistoryIQ) {
                            ChatHistoryIQ chatHistoryIQ = (ChatHistoryIQ) packet;
                            Log.e("report", chatHistoryIQ.getData());
                            Type type = new TypeToken<List<HistoryModel>>() {
                            }.getType();
                            try {
                                historyMessage = (ArrayList<HistoryModel>) parseResponse(chatHistoryIQ.getData(), type);
                                //unReadCount.clear();
                                for (HistoryModel historyModel : historyMessage)
                                {
                                    if(unReadCount.containsKey(historyModel.getFromJid())){
                                        try {
                                            unReadCount.remove(historyModel.getFromJid());

                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        unReadCount.put(historyModel.getFromJid(), Integer.valueOf(historyModel.getCount()));
                                    }
                                }
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {

                                        rosterList = databaseHelper.getUsers();
                                        userAdapter.setRosterList(rosterList);
                                        userAdapter.notifyDataSetChanged();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }
    }

    private static Gson getGson() {
        return new GsonBuilder().create();
    }


    public static Object parseResponse(String mResponse, Type typeOfClass) throws Exception {


        return getGson().fromJson(mResponse, typeOfClass);

    }

    @Override
    public void presenceUpdate(Presence presence) {
        if (presenceUser.containsKey(presence.getFrom().split("/")[0])) {
            Boolean present = true;
            try {
                presenceUser.remove(presence.getFrom().split("/")[0]);
                String type = presence.getType().name();
                if (type.equals("unavailable")) {
                    present = false;
                } else {
                    present = true;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            presenceUser.put(presence.getFrom().split("/")[0], present);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    rosterList = databaseHelper.getUsers();
                    userAdapter.setRosterList(rosterList);
                    userAdapter.notifyDataSetChanged();

                }
            });
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        userAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver,
                                                                                    new IntentFilter(Constants.GET_MESSAGE));
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(connectionReceiver,
                                                                                    new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(connectionReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            ChatMessageModel chatMessageModel = (ChatMessageModel) intent.getSerializableExtra(Constants.RECECEIVED_FROM);
            if (chatMessageModel != null) {
                userAdapter.notifyDataSetChanged();
            }
        }
    };

    BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(MyXMPP.connection.isConnected()){
                Presence presencePacket;
                if(LoginActivity.isConnectingToInternet(getApplicationContext())){
                    presencePacket = new Presence(Presence.Type.available);
                }
                else {
                    presencePacket = new Presence(Presence.Type.unavailable);
                }
                try
                {
                    MyXMPP.connection.sendStanza(presencePacket);
                }
                catch(SmackException.NotConnectedException e)
                {
                    e.printStackTrace();
                }
            }
        }
    };
}
