package com.aurigait.ejabberdchat;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ChatMessageModel> chatList;
    private String mSenderUser, lastDate;
    private boolean dateVisibility;

    public ChatAdapter(Activity context, ArrayList<ChatMessageModel> chatList, String mSenderUser) {

        this.context = context;
        this.chatList = chatList;
        this.mSenderUser = mSenderUser;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (position == 0) {
            lastDate = getDateFromTimeMillis(Long.parseLong(chatList.get(position).getMessageSentTime()));
            holder.time_date.setVisibility(View.VISIBLE);
            holder.time_date.setText(lastDate);
        } else {
            lastDate = getDateFromTimeMillis(Long.parseLong(chatList.get(position - 1).getMessageSentTime()));
            if (lastDate.equals(getDateFromTimeMillis(Long.parseLong(chatList.get(position).getMessageSentTime())))) {
                holder.time_date.setVisibility(View.GONE);
            } else {
                lastDate = getDateFromTimeMillis(Long.parseLong(chatList.get(position).getMessageSentTime()));
                holder.time_date.setVisibility(View.VISIBLE);
                holder.time_date.setText(lastDate);
            }
        }


        if (chatList.get(position).getMessageFrom().split("/")[0].split("@")[0].equals(mSenderUser.split("/")[0].split("@")[0])) {
            holder.right_layout.setVisibility(View.VISIBLE);
            holder.left_layout.setVisibility(View.GONE);
            holder.txt_right.setText(chatList.get(position).getMessage());
            holder.name_right.setText(mSenderUser.split("/")[0].split("@")[0]);
            if (chatList.get(position).getMessageStatus().equals(Constants.MESSAGE_READ)) {
                holder.imageView.setImageResource(R.drawable.chkd3);
            } else {
                holder.imageView.setImageResource(R.drawable.chkd1);
            }
            holder.time_right.setText(getTime(Long.parseLong(chatList.get(position).getMessageSentTime())));
            String readTime = chatList.get(position).getMessageReadTime();
            if (!readTime.equals(""))
                holder.time_read_right.setText(getTime(Long.parseLong(readTime)));
        } else {
            holder.left_layout.setVisibility(View.VISIBLE);
            holder.right_layout.setVisibility(View.GONE);
            holder.text_name_left.setText(chatList.get(position).getMessageFrom().split("/")[0].split("@")[0]);
            holder.txt_left.setText(chatList.get(position).getMessage());
            holder.time_left.setText(getTime(Long.parseLong(chatList.get(position).getMessageSentTime())));
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout left_layout, right_layout;
        private TextView txt_right, txt_left, time_right, time_left, time_date, time_read_right, name_right, text_name_left;
        private ImageView imageView, iv_profile_left, iv_profile_right;

        public ViewHolder(View itemView) {
            super(itemView);

            left_layout = (RelativeLayout) itemView.findViewById(R.id.left_layout);
            right_layout = (RelativeLayout) itemView.findViewById(R.id.right_layout);
            txt_left = (TextView) itemView.findViewById(R.id.txt_left);
            txt_right = (TextView) itemView.findViewById(R.id.txt_right);

            time_read_right = (TextView) itemView.findViewById(R.id.time_read_right);
            name_right = (TextView) itemView.findViewById(R.id.name_right);
            text_name_left = (TextView) itemView.findViewById(R.id.text_name_left);

            imageView = (ImageView) itemView.findViewById(R.id.image_left);

            iv_profile_left = (ImageView) itemView.findViewById(R.id.iv_profile_left);
            iv_profile_right = (ImageView) itemView.findViewById(R.id.iv_profile_right);

            time_right = (TextView) itemView.findViewById(R.id.sender_time);
            time_left = (TextView) itemView.findViewById(R.id.receiver_time);
            time_date = (TextView) itemView.findViewById(R.id.date_text);

        }
    }

    public void add(ChatMessageModel chatMessageDto) {

        chatList.add(chatMessageDto);
    }

    public void updateListItem(ArrayList<ChatMessageModel> chatList) {
        this.chatList = chatList;
    }

    public String getDateFromTimeMillis(long time) {
        String date = null;
        Date newDate = new Date(time);

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        if (DateUtils.isToday(time))
            return "Today";

        else
            return format.format(newDate);
    }

    private String getTime(long messageSentTime) {
        Date date = new Date(messageSentTime);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        return simpleDateFormat.format(date).split(" ")[1];
    }
}
