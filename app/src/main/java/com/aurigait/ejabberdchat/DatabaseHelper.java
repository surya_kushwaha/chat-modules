package com.aurigait.ejabberdchat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper
{

	private static DatabaseHelper databaseHelper;
	private SQLiteDatabase database;
	private static final String DATABASE_NAME = "ejabberd_chat_db";

	private static final int DATABASE_VERSION = 1;

	private static final String TABLE_USERS = "table_users";

	private static String JID = "jid";

	private static String NAME = "name";

	private String CREATE_TABLE_USERS = "CREATE TABLE "
			+ TABLE_USERS + "(" + JID + " TEXT PRIMARY KEY,"
			+ NAME + " TEXT )";


	private static final String TABLE_MESSAGE = "table_message";

	private static final String UNIQUE_ID = "unique_id";
	private static final String MESSAGE = "message";
	private static final String MESSAGE_ID = "message_id";
	private static final String MESSAGE_FROM = "message_from";
	private static final String MESSAGE_TO = "message_to";
	private static final String MESSAGE_DELIVERED_TIME = "message_delivered_time";
	private static final String MESSAGE_READ_TIME = "message_read_time";
	private static final String MESSAGE_SENT_TIME = "message_sent_time";
	private static final String MESSAGE_STATUS = "message_status";
	private static final String MESSAGE_TYPE = "message_type";

	private String CREATE_TABLE_MESSAGES = "CREATE TABLE "
			+ TABLE_MESSAGE + "(" + UNIQUE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ MESSAGE + " TEXT,"
			+ MESSAGE_ID + " TEXT,"
			+ MESSAGE_FROM + " TEXT,"
			+ MESSAGE_TO + " TEXT,"
			+ MESSAGE_DELIVERED_TIME + " TEXT,"
			+ MESSAGE_READ_TIME + " TEXT,"
			+ MESSAGE_SENT_TIME + " TEXT,"
			+ MESSAGE_TYPE + " TEXT,"
			+ MESSAGE_STATUS + " TEXT )";

	private DatabaseHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
// singleton class object
	public static DatabaseHelper getInstanse(Context context){

		if(databaseHelper == null){
			databaseHelper = new DatabaseHelper(context);
		}
		return databaseHelper;
	}

	public void onCreate(SQLiteDatabase paramSQLiteDatabase)
	{
		paramSQLiteDatabase.execSQL(CREATE_TABLE_USERS);
		paramSQLiteDatabase.execSQL(CREATE_TABLE_MESSAGES);
	}
	
	public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int oldVersion, int newVersion)
	{
		paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_USERS);
		paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_MESSAGES);
	}

	public void insertFieldsValue(String jid, String name){

		try{
			ContentValues userValues = new ContentValues();

			userValues.put(JID, jid);
			userValues.put(NAME, name);

			database.insert(DatabaseHelper.TABLE_USERS, null, userValues);
		}
		catch (Exception e){
			Log.i("exception insert", e.toString());
		}
	}

	synchronized public void insertMessageFieldsValue(ChatMessageModel chatMessageModel){

		try{
			ContentValues messageValue = new ContentValues();

			messageValue.put(MESSAGE, chatMessageModel.getMessage());
			messageValue.put(MESSAGE_ID, chatMessageModel.getMessageId());
			messageValue.put(MESSAGE_FROM, chatMessageModel.getMessageFrom());

			messageValue.put(MESSAGE_TO, chatMessageModel.getMessageTo());
			messageValue.put(MESSAGE_DELIVERED_TIME, chatMessageModel.getMessageDeliveredTime());
			messageValue.put(MESSAGE_READ_TIME, chatMessageModel.getMessageReadTime());

			messageValue.put(MESSAGE_SENT_TIME, chatMessageModel.getMessageSentTime());
			messageValue.put(MESSAGE_STATUS, chatMessageModel.getMessageStatus());
			messageValue.put(MESSAGE_TYPE, chatMessageModel.getMessageType());

			database.insert(DatabaseHelper.TABLE_MESSAGE, null, messageValue);
		}
		catch (Exception e){
			Log.i("exception insert", e.toString());
		}
	}

	synchronized public ArrayList<ChatMessageModel> getMessages(String receiver){
		ArrayList<ChatMessageModel> messageList = new ArrayList<>();

		try {
			String selectQuery = "SELECT  * FROM " + TABLE_MESSAGE +" where "+MESSAGE_TO+"='"+receiver+"' or "+MESSAGE_FROM+"='"+receiver+"' " +
					"ORDER BY "+ MESSAGE_SENT_TIME+" ASC";
			Cursor cursor = database.rawQuery(selectQuery, null);

			if (cursor.moveToFirst())
			{
				do
				{	ChatMessageModel chatMessageModel = new ChatMessageModel();

					chatMessageModel.setMessageFrom(cursor.getString(cursor.getColumnIndex(MESSAGE_FROM)));
					chatMessageModel.setMessageTo(cursor.getString(cursor.getColumnIndex(MESSAGE_TO)));
					chatMessageModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
					chatMessageModel.setMessageId(cursor.getString(cursor.getColumnIndex(MESSAGE_ID)));
					chatMessageModel.setMessageReadTime(cursor.getString(cursor.getColumnIndex(MESSAGE_READ_TIME)));
					chatMessageModel.setMessageDeliveredTime(cursor.getString(cursor.getColumnIndex(MESSAGE_DELIVERED_TIME)));
					chatMessageModel.setMessageSentTime(cursor.getString(cursor.getColumnIndex(MESSAGE_SENT_TIME)));
					chatMessageModel.setMessageStatus(cursor.getString(cursor.getColumnIndex(MESSAGE_STATUS)));
					chatMessageModel.setMessageType(cursor.getString(cursor.getColumnIndex(MESSAGE_TYPE)));
					messageList.add(chatMessageModel);
				}
				while (cursor.moveToNext());
			}
			cursor.close();

		}
		catch (Exception e){
			Log.i("exception in get users", e.toString());
		}

		return messageList;
	}

	synchronized public ArrayList<ChatMessageModel> getUnreadMessages(String receiver){
		ArrayList<ChatMessageModel> messageList = new ArrayList<>();

		try {
			//String selectQuery = "SELECT  * FROM " + TABLE_MESSAGE +" where "+MESSAGE_STATUS+"<>'"+ Constants.MESSAGE_READ+"'";
			String selectQuery = "SELECT  * FROM " + TABLE_MESSAGE +" where "+MESSAGE_STATUS+"!='"+ Constants.MESSAGE_READ+"'"
					+" and "+MESSAGE_FROM+"='"+receiver+"'";
			Cursor cursor = database.rawQuery(selectQuery, null);

			if (cursor.moveToFirst())
			{
				do
				{	ChatMessageModel chatMessageModel = new ChatMessageModel();

					chatMessageModel.setMessageFrom(cursor.getString(cursor.getColumnIndex(MESSAGE_FROM)));
					chatMessageModel.setMessageTo(cursor.getString(cursor.getColumnIndex(MESSAGE_TO)));
					chatMessageModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
					chatMessageModel.setMessageId(cursor.getString(cursor.getColumnIndex(MESSAGE_ID)));
					chatMessageModel.setMessageReadTime(cursor.getString(cursor.getColumnIndex(MESSAGE_READ_TIME)));
					chatMessageModel.setMessageDeliveredTime(cursor.getString(cursor.getColumnIndex(MESSAGE_DELIVERED_TIME)));
					chatMessageModel.setMessageSentTime(cursor.getString(cursor.getColumnIndex(MESSAGE_SENT_TIME)));
					chatMessageModel.setMessageStatus(cursor.getString(cursor.getColumnIndex(MESSAGE_STATUS)));
					chatMessageModel.setMessageType(cursor.getString(cursor.getColumnIndex(MESSAGE_TYPE)));
					messageList.add(chatMessageModel);
				}
				while (cursor.moveToNext());
			}
			cursor.close();

		}
		catch (Exception e){
			Log.i("exception in get users", e.toString());
		}

		return messageList;
	}

	synchronized public ChatMessageModel getLastMessages(String receiver){
		ChatMessageModel chatMessageModel = null;

		try {
			String selectQuery = "SELECT  * FROM " + TABLE_MESSAGE +" where "+MESSAGE_TO+"='"+receiver+"' or "+MESSAGE_FROM+"='"+receiver+"' " +
					"ORDER BY "+ MESSAGE_SENT_TIME+" ASC";
			Cursor cursor = database.rawQuery(selectQuery, null);

			if (cursor.moveToLast())
			{
					chatMessageModel = new ChatMessageModel();
					chatMessageModel.setMessageFrom(cursor.getString(cursor.getColumnIndex(MESSAGE_FROM)));
					chatMessageModel.setMessageTo(cursor.getString(cursor.getColumnIndex(MESSAGE_TO)));
					chatMessageModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
					chatMessageModel.setMessageId(cursor.getString(cursor.getColumnIndex(MESSAGE_ID)));
					chatMessageModel.setMessageReadTime(cursor.getString(cursor.getColumnIndex(MESSAGE_READ_TIME)));
					chatMessageModel.setMessageDeliveredTime(cursor.getString(cursor.getColumnIndex(MESSAGE_DELIVERED_TIME)));
					chatMessageModel.setMessageSentTime(cursor.getString(cursor.getColumnIndex(MESSAGE_SENT_TIME)));
					chatMessageModel.setMessageStatus(cursor.getString(cursor.getColumnIndex(MESSAGE_STATUS)));
					chatMessageModel.setMessageType(cursor.getString(cursor.getColumnIndex(MESSAGE_TYPE)));

			}
			cursor.close();
		}
		catch (Exception e){
			Log.i("exception in get users", e.toString());
		}

		return chatMessageModel;
	}

	synchronized public ChatMessageModel getMessageDetailsFromMessageID(String msgId){
		ChatMessageModel chatMessageModel = null;

		try {
			String selectQuery = "SELECT  * FROM " + TABLE_MESSAGE +" where "+MESSAGE_ID+"='"+msgId+"'";
			Cursor cursor = database.rawQuery(selectQuery, null);

			if (cursor.moveToFirst())
			{
				chatMessageModel = new ChatMessageModel();
				chatMessageModel.setMessageFrom(cursor.getString(cursor.getColumnIndex(MESSAGE_FROM)));
				chatMessageModel.setMessageTo(cursor.getString(cursor.getColumnIndex(MESSAGE_TO)));
				chatMessageModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
				chatMessageModel.setMessageId(cursor.getString(cursor.getColumnIndex(MESSAGE_ID)));
				chatMessageModel.setMessageReadTime(cursor.getString(cursor.getColumnIndex(MESSAGE_READ_TIME)));
				chatMessageModel.setMessageDeliveredTime(cursor.getString(cursor.getColumnIndex(MESSAGE_DELIVERED_TIME)));
				chatMessageModel.setMessageSentTime(cursor.getString(cursor.getColumnIndex(MESSAGE_SENT_TIME)));
				chatMessageModel.setMessageStatus(cursor.getString(cursor.getColumnIndex(MESSAGE_STATUS)));
				chatMessageModel.setMessageType(cursor.getString(cursor.getColumnIndex(MESSAGE_TYPE)));

			}
			cursor.close();

		}
		catch (Exception e){
			Log.i("exception in get users", e.toString());
		}

		return chatMessageModel;
	}

	synchronized public void updateMessage(String id, String status, String readTime, String sentTime, String deliveredTime) {
		try{
			ContentValues tableMessage = new ContentValues();

			if(!readTime.isEmpty())
				tableMessage.put(MESSAGE_READ_TIME, readTime);
			if(!sentTime.isEmpty())
				tableMessage.put(MESSAGE_SENT_TIME, sentTime);
			if(!deliveredTime.isEmpty())
				tableMessage.put(MESSAGE_DELIVERED_TIME, deliveredTime);

			tableMessage.put(MESSAGE_STATUS, status);

			int uid = database.update(DatabaseHelper.TABLE_MESSAGE, tableMessage, MESSAGE_ID + " = ?",
					new String[]{id});
			Log.i("update id", "> " + id);
		}
		catch (Exception e){
			Log.i("exception update", e.toString());
		}
	}

	synchronized public void updateMessageReadType(String id, String type) {
		try{
			ContentValues tableMessage = new ContentValues();

			tableMessage.put(MESSAGE_TYPE, type);

			int uid = database.update(DatabaseHelper.TABLE_MESSAGE, tableMessage, MESSAGE_FROM + " = ?",
					new String[]{id});
			Log.i("update id", "> " + id);
		}
		catch (Exception e){
			Log.i("exception update", e.toString());
		}
	}

	synchronized public boolean checkExistanceJID(String jid){

		String selectQuery = "SELECT * FROM " + TABLE_USERS +" where "+JID+"='"+jid+"'";

		boolean isExist = false;

		try {

			Cursor cursor = database.rawQuery(selectQuery, null);

			if(cursor != null && cursor.getCount()>0){
				isExist = true;
			}

			cursor.close();

		}
		catch (Exception e){
			Log.i("chexkExistance", e.toString());
		}
		return isExist;
	}

	synchronized public void updateUsers(String jid, String name) {
		try{
			ContentValues tableUsers = new ContentValues();

			tableUsers.put(NAME, name);

			int uid = database.update(DatabaseHelper.TABLE_USERS, tableUsers, JID + " = ?",
					new String[]{jid});
			Log.i("update id", "> " + uid);
		}
		catch (Exception e){
			Log.i("exception update", e.toString());
		}
	}

	synchronized public ArrayList<Users> getUsers(){
		ArrayList<Users> userList = new ArrayList<>();

		try {
			String selectQuery = "SELECT  * FROM " + TABLE_USERS ;
			Cursor cursor = database.rawQuery(selectQuery, null);

			if (cursor.moveToFirst())
			{
				do
				{	Users rosterDto = new Users();
					rosterDto.setJabberID(cursor.getString(cursor.getColumnIndex(JID)));
					rosterDto.setName(cursor.getString(cursor.getColumnIndex(NAME)));

					userList.add(rosterDto);
				}
				while (cursor.moveToNext());
			}
			cursor.close();

		}
		catch (Exception e){
			Log.i("exception in get users", e.toString());
		}

		return userList;
	}

	synchronized public Users getUserById(String jid){
		Users rosterDto = new Users();
		try {
			String selectQuery = "SELECT  * FROM " + TABLE_USERS +" where "+JID+"='"+jid+"'";

			Cursor cursor = database.rawQuery(selectQuery, null);

			if (cursor!= null && cursor.moveToFirst())
			{
				rosterDto.setName(cursor.getString(cursor.getColumnIndex(NAME)));
				rosterDto.setJabberID(jid);
			}
			cursor.close();

		}
		catch (Exception e){
			Log.i("exception getUser", e.toString());
		}
		return rosterDto;
	}

	public void openDatabase(){
			try{
				database = databaseHelper.getWritableDatabase();
			}catch (Exception e){
				Log.e("database exception", e.toString());
			}
	}

	public void closeDatabase(){
		try{
			databaseHelper.close();
		}catch (Exception e){
			Log.e("database exception", e.toString());
		}
	}
}
