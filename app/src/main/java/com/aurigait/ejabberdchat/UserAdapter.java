package com.aurigait.ejabberdchat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Pragya Mendiratta on 4/12/2016.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Users> rosterList;

    public UserAdapter(Context context, ArrayList<Users> rosterList) {

        this.context = context;
        this.rosterList = rosterList;

    }

    public void setRosterList(ArrayList<Users> rosterList){
        this.rosterList = rosterList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.roster_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.txt_name.setText(rosterList.get(position).getName());
        final ChatMessageModel chatMessageModel = DatabaseHelper.getInstanse(context).getLastMessages(rosterList.get(position).getJabberID().split("/")[0].split("@")[0]);

        if(chatMessageModel == null){
            holder.text_message.setVisibility(View.INVISIBLE);
            holder.text_time.setVisibility(View.INVISIBLE);
        }
        else
        {
            if(chatMessageModel.getMessageType().equals(Constants.MESSAGE_TYPE_NEW))
            {
                holder.text_message.setTextColor(Color.RED);
                holder.text_message.setCompoundDrawablesWithIntrinsicBounds( 0, 0, 0, 0);
            }
            else
            {
                if (chatMessageModel.getMessageTo().split("/")[0].split("@")[0].equals(rosterList.get(position).getJabberID().split("/")[0].split("@")[0])) {
                    if(chatMessageModel.getMessageStatus().equals(Constants.MESSAGE_READ)){
                        holder.text_message.setCompoundDrawablesWithIntrinsicBounds( 0, 0, R.drawable.read, 0);
                    }
                    else {
                        holder.text_message.setCompoundDrawablesWithIntrinsicBounds( 0, 0, R.drawable.check, 0);
                    }
                }
                else {
                    holder.text_message.setCompoundDrawablesWithIntrinsicBounds( 0, 0, 0, 0);
                }
                holder.text_message.setTextColor(Color.BLACK);
            }

            holder.text_time.setVisibility(View.VISIBLE);
            holder.text_message.setVisibility(View.VISIBLE);

            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(Long.parseLong(chatMessageModel.getMessageSentTime()));

            Date date = c.getTime();
            if(DateUtils.isToday(Long.parseLong(chatMessageModel.getMessageSentTime()))){
                String minutes = String.valueOf(date.getMinutes()).length()==1?"0"+ String.valueOf(date.getMinutes()): String.valueOf(date.getMinutes());
                holder.text_time.setText(String.valueOf(date.getHours()) +":" + minutes);
            }
            else {
                String minutes = String.valueOf(date.getMinutes()).length()==1?"0"+ String.valueOf(date.getMinutes()): String.valueOf(date.getMinutes());
                holder.text_time.setText(format.format(date)+" "+ String.valueOf(date.getHours())+":" + minutes);
            }

            holder.text_message.setText(chatMessageModel.getMessage());
        }

        if(MainActivity.presenceUser != null && MainActivity.presenceUser.size()>0)
        {
            if(MainActivity.presenceUser.containsKey(rosterList.get(position).getJabberID())){
                if(MainActivity.presenceUser.get(rosterList.get(position).getJabberID())){
                    holder.imagePresence.setImageResource(R.drawable.green_dot);
                }
                else {
                    holder.imagePresence.setImageResource(R.drawable.away);
                }
            }
        }

        if(MainActivity.unReadCount != null && MainActivity.unReadCount.size()>0)
        {
            if(MainActivity.unReadCount.containsKey(rosterList.get(position).getJabberID().split("/")[0].split("@")[0]))
            {
                Integer count = MainActivity.unReadCount.get(rosterList.get(position).getJabberID().split("/")[0].split("@")[0]);
                if(count>0){
                    holder.unread_count.setVisibility(View.VISIBLE);
                    holder.unread_count.setTextColor(Color.RED);
                    holder.unread_count.setText(String.valueOf(count));
                }else {
                    holder.unread_count.setVisibility(View.INVISIBLE);
                }
            }
        }

        holder.roster_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("user",rosterList.get(position));

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rosterList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_name,text_message,text_time,unread_count;
        private ImageView img_profile,imagePresence;
        private RelativeLayout roster_layout;

        public ViewHolder(View itemView) {
            super(itemView);

                    text_time = (TextView) itemView.findViewById(R.id.txt_time);
                    text_message = (TextView) itemView.findViewById(R.id.txt_last_msg);
                    txt_name = (TextView) itemView.findViewById(R.id.txt_name);
                    unread_count = (TextView) itemView.findViewById(R.id.unread_count);
                    img_profile = (ImageView) itemView.findViewById(R.id.img_profile);
                    imagePresence = (ImageView) itemView.findViewById(R.id.imageView);
                    roster_layout = (RelativeLayout) itemView.findViewById(R.id.roster_layout);
        }
    }

}
