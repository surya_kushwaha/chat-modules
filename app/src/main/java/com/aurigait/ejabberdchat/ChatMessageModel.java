package com.aurigait.ejabberdchat;

import java.io.Serializable;

public class ChatMessageModel implements Serializable
{
    private String message,messageId,messageFrom,messageTo,messageReadTime,messageDeliveredTime,
            messageSentTime,messageStatus,messageType;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(String messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(String messageTo) {
        this.messageTo = messageTo;
    }

    public String getMessageReadTime() {
        return messageReadTime;
    }

    public void setMessageReadTime(String messageReadTime) {
        this.messageReadTime = messageReadTime;
    }

    public String getMessageDeliveredTime() {
        return messageDeliveredTime;
    }

    public void setMessageDeliveredTime(String messageDeliveredTime) {
        this.messageDeliveredTime = messageDeliveredTime;
    }

    public String getMessageSentTime() {
        return messageSentTime;
    }

    public void setMessageSentTime(String messageSentTime) {
        this.messageSentTime = messageSentTime;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
}
