package com.aurigait.ejabberdchat;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//import de.meisterfuu.smackdemo.service.SmackService;

/**
 * Created by auriga on 27/2/16.
 */
public class Registration implements PingFailedListener
{
    private String userid,password,email;
    String serviceName,username;
    private Context context;
    private XMPPTCPConnection connection;

    private boolean Active;
    private java.lang.Thread Thread;
    private Handler THandler;
    private boolean success=false;


    public Registration(Context context, String userid, String password, String email)
    {
        this.userid=userid;
        this.password=password;
        this.email=email;
        this.context=context;
        serviceName=userid.split("@")[1];
        username=userid.split("@")[0];

    }
    public boolean perform()
    {
        if (!Active) {
            Active = true;

            // Create ConnectionThread Loop
            if (Thread == null || !Thread.isAlive()) {
                Thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        THandler = new Handler();
                        try
                        {
                            createAccount(); Log.e("SIGN UP START", "SIGN UP .....");
                            SignUp.processcompleted=true;
                            SignUp.result=true;
                        }
                        catch (Exception e)
                        {
                            Log.e("Error", "FAILED");
                            success=false;
                            SignUp.processcompleted=true;
                        }

                        Looper.loop();
                    }

                });
                Thread.start();
            }
        }
        return success;
    }
    public void createAccount() throws IOException, XMPPException, SmackException
    {

        XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration
                .builder();
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        config.setServiceName("localhost");
        config.setResource("SmackAndroidTestClient");
        config.setHost("192.168.3.89");
        config.setPort(5222);
        config.setDebuggerEnabled(true);
        config.setCompressionEnabled(false);
        config.setUsernameAndPassword("admin", "admin");
        XMPPTCPConnection.setUseStreamManagementResumptiodDefault(true);
        XMPPTCPConnection.setUseStreamManagementDefault(true);
        connection = new XMPPTCPConnection(config.build());

        //Set ConnectionListener here to catch initial connect();
        connection.addConnectionListener(new ConnectionListener()
        {
            @Override
            public void connected(XMPPConnection connection)
            {

            }

            @Override
            public void authenticated(XMPPConnection connection, boolean resumed)
            {
                AccountManager accountManager= AccountManager.getInstance(connection);
                try {
                    // accountManager.createAccount("auriga@localhost", "user123");
                    /*Map<String, String> map = new HashMap<String, String>();
                    map.put("username", username);
                    map.put("password", password);
                    map.put("email", email);
                    map.put("name","Default Name");
                    accountManager.sensitiveOperationOverInsecureConnection(true);
                    accountManager.createAccount("admin", "admin",map);*/
                    if (accountManager.supportsAccountCreation()) {
                        accountManager.sensitiveOperationOverInsecureConnection(true);
                        accountManager.createAccount("userName", "password");

                    }
                    SignUp.result=true;
                    Log.e("SUCCESS", "DONE");
                    Toast.makeText(context, context.getString(R.string.signupsuccess), Toast.LENGTH_LONG).show();

                    //    connection.disconnect();
                    //  return true;


                } catch (XMPPException e1) {
                    Log.e("Errorr 1", e1.getMessage());
                    SignUp.result=false; //   success=false;
                    //  Toast.makeText(getClass().getPackage().,"XMPP Connection Error",Toast.LENGTH_SHORT).show();
                    //  return false;
                }
                catch (SmackException.NoResponseException e1) {
                    Log.e("Errorr 2", e1.getMessage());
                    SignUp.result=false;  // success=false;
                    //return false;

                }catch (SmackException.NotConnectedException e1) {
                    Log.e("Errorr 3", e1.getMessage());
                    SignUp.result=false;
                    //  success=false;
                    //return false;
                }

            }

            @Override
            public void connectionClosed()
            {

            }

            @Override
            public void connectionClosedOnError(Exception e)
            {

            }

            @Override
            public void reconnectionSuccessful()
            {

            }

            @Override
            public void reconnectingIn(int seconds)
            {

            }

            @Override
            public void reconnectionFailed(Exception e)
            {

            }
        });


        connection.connect();


        PingManager.setDefaultPingInterval(600); //Ping every 10 minutes
        PingManager pingManager = PingManager.getInstanceFor(connection);
        pingManager.registerPingFailedListener(this);

    }

    @Override
    public void pingFailed() {

    }
}
